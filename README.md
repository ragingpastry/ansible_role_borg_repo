ansible_role_borg_repo
=========

This role will setup a borg repo server using borg and a wrapper script called postjob_runner.

What is Borg?
BorgBackup is a deduplicating backup program. Optionally, it supports compression and authenticated encryption.

What is postjob_runner?
In short, the postjob_runner runs post backup jobs that are defined on the backup client side. After the borg client successfully completes a borg backup, it will submit a user defined post job to the backup repo. Every minute the postjob_runner will check the queue for post job request and execute it. This is useful for testing backups.

Requirements
------------

git, awk, and sed must be installed

Role Variables
--------------

```
borg_required_packages:
  yum:
    - borgbackup
    - ansible
    - git

postjob_runner_log_directory: /var/log/borgbackup # specify where to store logs
backup_user: borg # specify the user account to run borg under
borg_repo_path: /backups # specify the location of where to store borg client repos
postjob_runner_install_path: /usr/local/borgbackup # specify where to install the postjob runner
borg_home_directory: /home/borg # specify the location the borg postjob runner's home directory
borg_repo_hostname: backup_repo # specify the hostname of the backup repo.
borg_repo_generate_secrets: false # set to enable when generating new secrets.
install_postjob_runner: true # set to true to install the post job runner
clean_secrets_files: false # delete temporary password files
postjob_runner_queue_config_name: postjob_runner_config.yml # specify the name of the config file
postjob_runner_queue_path: /backups/postjobs # specify the location of the client post job requests.
postjob_runner_queue_check_interval: "*" # set to every minute
postjob_runner_executer: ansible # set type of executor to run
postjob_working_directory: /home/borg/playbooks # specify the location of the post job playbooks.

borg_repo_ssh_priv_key: |
    -----BEGIN OPENSSH PRIVATE KEY-----
    -----END OPENSSH PRIVATE KEY-----

borg_repo_ssh_pub_key: |
    ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCxJ78jgJj/ppmYKLgaCpJiSMz7wJe2c+rAAfBAh8SOfVMC0OaFawp+IWl4CAD2UqPiR+DgYqfW3t65OF2gQhJJ0u2iTsEklpFUKrgbE/B7LZFQ5Q8ZAeET4BnP7R3Aznn2F600Yv2L4xt303/Zx3dLsT3oT6RoUNPA+N4+na8J9PGpHXcqChhFiXalqXpOD0al1dekPr2Q1TRgmDArTiB1P6I8PNqi4VbwlnNCV99qfjsyf2xVTZ12Scf7e4vS2lpiLz46DJx//iZgxGlHLsTZvjISs9yLgkhDYVHzlGrwidSOCSl1LdhZLCajlnEq4kW3IXtaPzAPqUaHxr2nzZPf briguy@linux-b6lq

borg_pass: |
    xxxxxxxxxxxxxxx
```

Dependencies
------------

```
ansible_role_packages
ansible_role_repository

```

Example Playbook
----------------

```
- hosts: borg_clients
  roles:
     -  role: ansible_role_borg_repo
```

License
-------

BSD

Author Information
------------------

The Development Range Engineering, Architecture, and Modernization (DREAM) Team
